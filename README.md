# siteChecker
This is a Python script used for checking website status. Outages are reported using Prowl.

## Requirements
In the same directory that you run siteChecker.py from, you also need the following:
- sites.txt - with a site to be checked on each line (must include http:// or https://)
- prowl.key - the first line must be your Prowl API key.
