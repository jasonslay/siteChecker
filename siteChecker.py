import requests


class Prowl():
    def __init__(self, keyfile='prowl.key', verify=False):
        self.baseUrl = 'https://api.prowlapp.com/publicapi/'
        with open(keyfile, 'r') as f:
            key = f.readlines()[0].strip()

        if verify:
            r = requests.get(self.baseUrl + 'verify', params={'apikey': key})
            if r.status_code == 200:
               self.key = key
            else:
                raise ValueError('API Key could not be found or is not valid!')
        else:
            self.key=key

    def send(self, application, event, description, priority=1, url=None):
        payload = {
            'apikey': self.key,
            'application': application,
            'event': event,
            'description': description,
            'priority': priority,
            'url': url
        }
        r = requests.post(self.baseUrl + 'add', data=payload)
        if r.status_code != 200:
            raise ValueError('Unable to send prowl message!')


# Prowl used to send messages
prowl = Prowl()


class Site():
    def __init__(self, url):
        self.url = url

    def check(self):
        if requests.get(self.url).status_code != 200:
            print("Couldn't connect to " + self.url + '!')
            prowl.send('siteChecker', self.url,
                       'site is down!', 2, self.url)
        else:
            print(self.url + ' is up!')


def main():
    with open('sites.txt', 'r') as f:
        file = f.readlines()
    sites = [line.strip() for line in file]
    # Filter empty lines and commented out sites
    sites = [site for site in sites if len(site) and site[0] != '#']
    for url in sites:
        print(url)
        Site(url).check()


if __name__ == '__main__':
    main()
